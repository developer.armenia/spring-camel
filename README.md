https://examples.javacodegeeks.com/enterprise-java/apache-camel/apache-camel-hello-world-example/

In this article, I am going to show you a ‘Hello World’ example of camel.

But first let me first introduce you to Apache Camel.

Apache Camel is an open-source integration framework which integrates systems. Messages play an important role in integrating systems, you can decide from which source to accept message, build the routing rules to determine how to process and send those messages to the other destinations.

Apache Camel uses URIs to work directly with any kind of Transport or messaging model such as HTTP, ActiveMQ, JMS etc., but on the outset, the API remains same regardless of the transport protocol the systems are using.

Camel has two main ways of defining routing rules: the Java-based domain specific language (DSL) and the Spring XML configuration format. I will show you an example for each case.

Now its the time to setup, below are the details: